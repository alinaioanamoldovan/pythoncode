import sqlite3
import numpy as np

db = sqlite3.connect('reuters.db')
cursor = db.cursor()
cursor.execute("SELECT a.term,b.term,SUM(a.count * b.count) FROM (SELECT * FROM Frequency  UNION SELECT  'q' as docid, 'washington' as term, 1 as count UNION SELECT  'q' as docid, 'taxes' as term, 1 as count UNION SELECT  'q' as docid, 'treasury' as term, 1 as count ) a, Frequency b WHERE a.term = b.term AND a.docid = 'q' GROUP BY b.docid, b.term ORDER BY SUM(a.count * b.count)")
result = cursor.fetchall()
for res in result:
	print res
