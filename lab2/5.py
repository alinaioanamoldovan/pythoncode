import sqlite3
import numpy

def getData(cursor,table):
	cursor.execute("SELECT * FROM "+table)
	data = cursor.fetchall()
	return data
def matrixMult(a,b):
	rows_a = len(a)
	cols_a = len(a[0])
	rows_b = len(b)
	cols_b = len(b[0])
	c = [[0 for row in range(cols_b)] for col in range(rows_a)]
	for i in range(rows_a):
		for j in range(cols_b):
			for k in range(cols_a):
				c[i][j] += a[i][k] * b[k][j]
	c = numpy.matrix(c).reshape(len(a),len(b[0]))

	return c
	 
db = sqlite3.connect('matrix.db')
cursor = db.cursor()
dataFromTableA = getData(cursor,"a")
dataFromTableB = getData(cursor,"b")
resultMultiplyMatrix = matrixMult(dataFromTableA,dataFromTableB)
print resultMultiplyMatrix