import sqlite3

def getData(cursor,table):
	cursor.execute("SELECT * FROM "+table)
	data = cursor.fetchall()
	return data
db = sqlite3.connect('matrix.db')
cursor = db.cursor()
cursor.execute("SELECT row_num, col_num, sum(prod) FROM (SELECT a.row_num as row_num, b.col_num as col_num, (a.value * b.value) prod FROM a, b WHERE a.col_num = b.row_num) GROUP BY row_num, col_num")
result = cursor.fetchall()
#print result

for res in result:
	print res