import json
import re
import string 

listOfWords = [ "I","you","he","she","he","we","him","us","as",
"at",
"but",
"by",
"down",
"for",
"from",
"in",
"into",
"this",
"like",
"near",
"next",
"of",
"off",
"on",
"onto",
"out",
"over",
"past",
"plus",
"minus",
"since",
"than",
"to",
"up",
"with","this","like","just","from","back","them","their","what","who","whom","that","your","have","This","it's","When","because","It's","doesn't","there","then","you're","we're"]

d = {}
with open("sentiment_scores.txt") as f:
    for line in f:
		val = line.split()
		d[val[0].decode('utf-8')] = int(val[1])
		#print val[0] 
		#print val[1]
	   
#print d   
tweets = []
for line in open('twitter_data1.txt', 'r'):
    tweets.append(json.loads(line))

frequency = {}
positiveTweets = {}
negativeTweets = {}
for line in tweets:
        # print "Before" + line
	score = 0
	line['text'] = re.sub(r'[.,"!?]+', '', line['text'], flags=re.MULTILINE)  # removes the characters specified
	line['text'] = re.sub(r'^RT[\s]+', '', line['text'], flags=re.MULTILINE)  # removes RT
	line['text'] = re.sub(r'https?:\/\/.*[\r\n]*', '', line['text'], flags=re.MULTILINE)  # remove link
	line['text'] = re.sub(r'[:]+', '', line['text'], flags=re.MULTILINE)
	line['text'] = re.sub(r'//+', '', line['text'], flags=re.MULTILINE)
	line['text'] = re.sub(r'^&amp;', '', line['text'], flags=re.MULTILINE)  # removes RT
	line['text'] = filter(lambda x: x in string.printable, line['text'])  # filter non-ascii characers

	new_line = ''
	for i in line['text'].split():# remove @ and #words, punctuataion
		if not i.startswith('@') and not i.startswith('#') and i not in string.punctuation and not i.startswith('&') and not i.startswith('(') and not i.startswith('[') and not i.startswith('-') and not i.startswith('$') and len(i)>3:
			new_line += i + ' '
	line['text'] = new_line
	#print line['text']
	for textTest in line['text'].split():
		f=open("derived_scores.txt","w")
		if textTest.decode('utf-8') in d.keys():
			score+=d[textTest]
			if textTest in frequency.keys():
				if  textTest not in listOfWords:
					frequency[textTest]+=1
			elif len(frequency) < 500:
					frequency[textTest] = 1
		else:
			if score > 0:
				f.write(textTest+' '+str(1))
			elif score < 0:
				f.write(textTest+' '+str(-1))
			else: f.write(textTest+' '+str(0))
		#print line['user']['friends_count']
		f.close()
	#print "The score of the sentiment %d " % score
	if score > 0:
		positiveTweets[line['user']['screen_name'].decode("utf-8")] = int(line['user']['friends_count'])
	else:
	    negativeTweets[line['user']['screen_name'].decode("utf-8")] = int(line['user']['friends_count'])
		
sorted_keys = sorted(frequency, key=frequency.get, reverse=True)
#for r in sorted_keys:
 #   print r, frequency[r]	
	
sorted_positiveTweets = sorted(positiveTweets, key=positiveTweets.get, reverse=True)
sorted_negativeTweets = sorted(negativeTweets, key=negativeTweets.get, reverse=True)
#for r in sorted_positiveTweets:
 #   print r, positiveTweets[r]
#for r in sorted_negativeTweets:
 #   print r, negativeTweets[r]
if positiveTweets[sorted_positiveTweets[0]] > negativeTweets[sorted_negativeTweets[0]]:
		print "Yes"
else: print "No"
