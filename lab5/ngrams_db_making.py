import sqlite3
from capstone import *
import pefile
import os
import hashlib

def find_ngrams(input_list, n):
  return zip(*[input_list[i:] for i in range(n)])

def get_decompiled_code(file):
	#with open(filename, 'rb') as content_file:
	#	content = content_file.read()
	pe = pefile.PE(file)
	opCodes = []
	syntax = 0
	try:
		md = Cs(CS_ARCH_X86, CS_MODE_32)
		md.detail = True

		if syntax != 0:
			md.syntax = syntax
		
		for section in range(pe.FILE_HEADER.NumberOfSections):
			if pe.sections[section].Characteristics & 0x00000020 == 0 or pe.sections[section].Characteristics & 0x20000000 > 0: 
				for insn in md.disasm(pe.sections[section].get_data(), 0x1000):
					#print "Here"
					#print insn
					if (insn.opcode[0] != 144) and (insn.opcode[0] != 204): 
						#print insn.opcode
						opCodes.append(insn.opcode[0])
	
	except CsError as e:
		print "ERROR: %s" % e

	return opCodes



frequency = {}
sqlite_file = 'features_raw.sqlite'
db2 = "features.sqlite"
conn = sqlite3.connect(sqlite_file)
sql_create_table = """CREATE TABLE IF NOT EXISTS homeworks (
                                    hash text,
                                    assignment_number TEXT,
                                    student_number TEXT,
                                    ngram text 
									);""" 
c = conn.cursor()
c.execute(sql_create_table)

conn2 = sqlite3.connect(db2)
c2 = conn2.cursor()
c2.execute(sql_create_table) 


#for item in my_list:
 # c.execute('insert into tablename values (?,?,?)', item)

dir_path = os.path.dirname(os.path.realpath(__file__)) 



for fisier in os.listdir(dir_path):
	print "Inceput for 1"
	filename = open(fisier,'rb')
	components =  filename.name.split(".")
	if components[1] == "exe":
		data = filename.read()
		print "Inceput for 1.1"
		hash = hashlib.md5(data).hexdigest()
		nume_fisier = components[0].split("_")
		assignment_number = nume_fisier[0]
		student_number = nume_fisier[1]
		ngram = find_ngrams(get_decompiled_code(fisier),5)
		lista = ""
		lista = ",".join("(%s,%s,%s,%s,%s)" % tup for tup in ngram)
		if lista in frequency:
			frequency[lista] = frequency[lista] + 1
		else:
			frequency[lista] =1
		c.execute("insert into homeworks  values (?,?,?,?)" ,(hash,assignment_number,student_number,lista))
		conn.commit()	
print "Out for 1"		
for  fisier in os.listdir(dir_path):
	print "Start for 2"
	filename = open(fisier	,'rb')
	components =  filename.name.split(".")
	if components[1] == "exe":
		print "Start for 2.1"
		data = filename.read()
		hash = hashlib.md5(data).hexdigest()
		nume_fisier = components[0].split("_")
		assignment_number = nume_fisier[0]
		print "Start for 2"
		student_number = nume_fisier[1]
		ngram = find_ngrams(get_decompiled_code(fisier),5)
		lista = ""
		lista = ",".join("(%s,%s,%s,%s,%s)" % tup for tup in ngram)
		if frequency[lista] < 30:
			print "diferit"
			c2.execute("insert into homeworks  values (?,?,?,?)" ,(hash,assignment_number,student_number,lista))
		else:
			print "egal"
			c2.execute("insert into homeworks  values (?,?,?)" ,(hash,assignment_number,student_number))
		conn2.commit()	
print "Out for 2"


