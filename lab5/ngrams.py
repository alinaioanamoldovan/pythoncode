import sqlite3
from capstone import *
import pefile
import os
import hashlib
from collections import defaultdict
import operator
import pprint

def sortDictionary(dict):
	for dic in dict.iterkeys():
		dict[dic] = sorted(dict[dic].items(), key=operator.itemgetter(1), reverse=True)
	return dict
	
def printDict(dict):
	for dic in dict.iterkeys():
		print dic 
		print dict[dic]
		print "\n"

def similarityValuefun(row1,row2):
	similarityValue = 0.0
	if row1 != None and row2 != None:
		similarityValue = 0.0
		row1 = list(row1)
		row2 = list(row2)
		ngram1 = row1[3]
		ngram2 = row2[3]
		intersection = 0.0
		for i in ngram1:
			if i in ngram2:
				intersection += 1.0
		reunion = len(ngram1)+ len(ngram2)
		similarityValue = 1 - intersection/reunion
		print similarityValue
	return similarityValue
	
def sim1(db,hash1,hash2):
	cursor = db.cursor()
	cursor2 = db.cursor()
	cursor.execute("SELECT * FROM homeworks WHERE hash = ?",(hash1, ))
	cursor2.execute("SELECT * FROM homeworks WHERE hash = ?",(hash2, ))
	row1 = cursor.fetchone()
	row2 = cursor2.fetchone()
	return similarityValuefun(row1,row2)
	
	
	
def sim2(db,assignment_number,student1,student2):
	c1 = db.cursor()
	c2 = db.cursor()
	c1.execute("SELECT * FROM homeworks WHERE assignment_number = ? AND student_number = ?",(assignment_number,student1))
	row1 = c1.fetchone()
	c2.execute("SELECT * FROM homeworks WHERE assignment_number = ? AND student_number = ?",(assignment_number,student2))
	row2 = c2.fetchone()
	return similarityValuefun(row1,row2)
		
def computeDictionary(lista,dict):
	print len(lista)
	for i in range(len(lista)):
		print i
		for j in range(i):
			print j
			dict[lista[i][2]][lista[j][2]] = sim2(conn,lista[i][1],lista[i][2],lista[j][2])

sqlite_file = 'features.sqlite'

conn = sqlite3.connect(sqlite_file)
c = conn.cursor()
c.execute("SELECT * FROM homeworks")
rows = c.fetchall()
a1 = defaultdict(dict)
a2 = defaultdict(dict)
a3 = defaultdict(dict)
a4 = defaultdict(dict)
a5 = defaultdict(dict)
a6 = defaultdict(dict)
a7 = defaultdict(dict)
a8 = defaultdict(dict)
a9 = defaultdict(dict)
a10 = defaultdict(dict)
lista_a1 = []
lista_a2 = []
lista_a3 = []
lista_a4 = []
lista_a5 = []
lista_a6 = []
lista_a7 = []
lista_a8 = []
lista_a9 = []
lista_a10 = []
for row in range(len(rows)):
	if rows[row][1] == "a01":
		lista_a1.append(rows[row])
	if rows[row][1] == "a02":
		lista_a2.append(rows[row])
	if rows[row][1] == "a03":
		lista_a3.append(rows[row])
	if rows[row][1] == "a04":
		lista_a4.append(rows[row])
	if rows[row][1] == "a05":
		lista_a5.append(rows[row])
	if rows[row][1] == "a06":
		lista_a6.append(rows[row])
	if rows[row][1] == "a07":
		lista_a7.append(rows[row])
	if rows[row][1] == "a08":
		lista_a8.append(rows[row])
	if rows[row][1] == "a09":
		lista_a9.append(rows[row])
	if rows[row][1] == "a10":
		lista_a10.append(rows[row])
computeDictionary(lista_a1,a1)
computeDictionary(lista_a2,a2)
computeDictionary(lista_a3,a3)
computeDictionary(lista_a4,a4)
computeDictionary(lista_a5,a5)
computeDictionary(lista_a6,a6)
computeDictionary(lista_a7,a7)
computeDictionary(lista_a8,a8)
computeDictionary(lista_a9,a9)
computeDictionary(lista_a10,a10)
a1 = sortDictionary(a1)
a2 = sortDictionary(a2)
a3 = sortDictionary(a3)
a4 = sortDictionary(a4)
a5 = sortDictionary(a5)
a6 = sortDictionary(a6)
a7 = sortDictionary(a7)
a8 = sortDictionary(a8)
a9 = sortDictionary(a9)
a10 = sortDictionary(a10)

print "For tema 1"
print "\n"
printDict(a1)
#print sortDictionary(a2)
#print sortDictionary(a3)
#print sortDictionary(a4)
#print sortDictionary(a5)
#print sortDictionary(a6)
#print sortDictionary(a7)
#print sortDictionary(a8)
#print sortDictionary(a9)
#print sortDictionary(a10)'''
