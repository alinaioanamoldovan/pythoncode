import pefile
from capstone import *


def similary(x,y):
	return 1 - x/y
	
def find_ngrams(input_list, n):
  return zip(*[input_list[i:] for i in range(n)])

def get_decompiled_code(section):
	#with open(filename, 'rb') as content_file:
	#	content = content_file.read()
	opCodes = []
	syntax = 0
	try:
		md = Cs(CS_ARCH_X86, CS_MODE_32)
		md.detail = True

		if syntax != 0:
			md.syntax = syntax
		
	
		for insn in md.disasm(section.get_data(), 0x1000):
			#print "Here"
			#print insn
			if (insn.opcode[0] != 144) and (insn.opcode[0] != 204): 
				#print insn.opcode
				opCodes.append(insn.opcode[0])
	
	except CsError as e:
		print "ERROR: %s" % e

	return opCodes

pe = pefile.PE("SimpleProject1.exe")
pe2 = pefile.PE("SimpleProject2.exe")
print "The platform %s " % hex(pe.FILE_HEADER.Machine)
print "The number of sections for the exe is %s" % hex(pe.FILE_HEADER.NumberOfSections)
print "The AddressOfEntryPoint for the exe is %s" % hex(pe.OPTIONAL_HEADER.AddressOfEntryPoint)
#print pe.dump_info()
numberOfExecutableSections = 0

ngrams51 = ()

for section in range(pe.FILE_HEADER.NumberOfSections):
	if pe.sections[section].Characteristics & 0x00000020 == 0 or pe.sections[section].Characteristics & 0x20000000 > 0: 
		#print pe.sections[section]
		opCodes1 = get_decompiled_code(pe.sections[section])
		#print pe.sections[section].get_data()
		numberOfExecutableSections += 1
		ngrams51	 = find_ngrams(opCodes1,5)
		
ngrams52 = ()
for section in range(pe2.FILE_HEADER.NumberOfSections):
	if pe2.sections[section].Characteristics & 0x00000020 == 0 or pe2.sections[section].Characteristics & 0x20000000 > 0: 
		#print pe.sections[section]
		opCodes1 = get_decompiled_code(pe.sections[section])
		#print pe.sections[section].get_data()
		ngrams52	 = find_ngrams(opCodes1,5)
#print "The number of executable sections for this exe is %d" % numberOfExecutableSections

positionOfTheEntryPoint = 0.0
for section in range(pe.FILE_HEADER.NumberOfSections):
	if (pe.sections[section].VirtualAddress <= pe.OPTIONAL_HEADER.AddressOfEntryPoint) and (pe.sections[section].VirtualAddress + pe.sections[section].SizeOfRawData >= pe.OPTIONAL_HEADER.AddressOfEntryPoint ):
		numberOfTheSection = section
		positionOfTheEntryPoint = (pe.OPTIONAL_HEADER.AddressOfEntryPoint / pe.sections[section]. SizeOfRawData ) / 10.0
print "The number of the section where the AddressOfEntryPoint is located for this exe is  %d " % numberOfTheSection
print "The position of the entry point in the section is %s " % positionOfTheEntryPoint


intersection = 0.0
for i in ngrams51:
	if i in ngrams52:
		intersection += 1.0
reunion = len(ngrams51)+ len(ngrams52)
similaryValue = similary(intersection,reunion)
print similaryValue
	
